import Foundation
import UIKit

class SignUpViewController: UIViewController, UINavigationControllerDelegate {
    
    @IBOutlet var activityindicator: UIActivityIndicatorView!
    @IBOutlet var firstName: UITextField!
    @IBOutlet var lastName: UITextField!
    @IBOutlet var mobileNumber: UITextField!
    @IBOutlet var emailID: UITextField!
    @IBOutlet var password: UITextField!
    @IBOutlet var confirmPassword: UITextField!
    @IBOutlet var DatePicker: UIDatePicker!
    var imflag=0;
    let imagePicker = UIImagePickerController()
    var DOB : String!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.hidden = false
        navigationItem.prompt = "Enter the following details"
        DatePicker.setValue(UIColor.whiteColor(), forKey: "textColor")
        DatePicker.datePickerMode = .DateAndTime
        DatePicker.datePickerMode = .Date
        DatePicker.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func registerUser(sender: UIButton) {
        sender.enabled=false
        var flag:Bool=true
        let firstName1:String = firstName.text!
        let lastName1:String = lastName.text!
        let mobileNumber1:String  = mobileNumber.text!
        let emailID1: String = emailID.text!
        let password1: String = password.text!
        let confirmPassword1: String = confirmPassword.text!
        let DOB:String = "1994-12-12"
        
        if firstName1 == "" || lastName1 == "" || mobileNumber1 == "" || emailID1 == "" || password1 == "" || DOB == ""  {
            flag = false
            let alertView:UIAlertView = UIAlertView()
            alertView.title = "Registration Failed!"
            alertView.message = "Please all the details"
            alertView.delegate = self
            alertView.addButtonWithTitle("OK")
            alertView.show()
            sender.enabled=true
            
        }
            
            
        else if password1 != confirmPassword1 {
            flag = false
            let alertView:UIAlertView = UIAlertView()
            alertView.title = "Error"
            alertView.message = "Passwords dont match"
            alertView.delegate = self
            alertView.addButtonWithTitle("OK")
            alertView.show()
            sender.enabled=true
            
        }
        if(flag)
        {
            activityindicator.startAnimating();
            activityindicator.sizeToFit()
            dispatch_async(dispatch_get_main_queue())
                {
                    
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd"
                    let date:NSDate = self.DatePicker.date
                    let post:NSString = "name=\(firstName1)&nickname=\(lastName1)&email=\(emailID1)&mobile=\(mobileNumber1)&password=\(password1)&dob=\(dateFormatter.stringFromDate(date))"
                    let addr:NSString="register"
                    let DataObj1:DataClass = DataClass();
                    let urlData: NSData? = DataObj1.JSONFetch(post as String, Address: addr as String)
                    if(urlData != nil)
                    {
                        let responseData:NSString  = NSString(data:urlData!, encoding:NSUTF8StringEncoding)!
                        NSLog(responseData as String)
                        let alertView:UIAlertView = UIAlertView()
                        alertView.title = "Welcome"
                        alertView.message = "Activate your account"
                        alertView.delegate = self
                        alertView.addButtonWithTitle("OK")
                        alertView.show()
                        self.navigationController?.popViewControllerAnimated(true)
                        self.dismissViewControllerAnimated(true, completion: nil)
                    }
                    else
                    {
                        if(Internet==0)
                        {
                            let alertView:UIAlertView = UIAlertView()
                            alertView.title = "Error!"
                            alertView.message = "Check your internet connection"
                            alertView.delegate = LoginViewController.self
                            alertView.addButtonWithTitle("OK")
                            alertView.show()
                        }
                        else
                        {
                            let alertView:UIAlertView = UIAlertView()
                            alertView.title = "Error!"
                            alertView.message = "Try again"
                            alertView.delegate = LoginViewController.self
                            alertView.addButtonWithTitle("OK")
                            alertView.show()
                        }
                        
                        
                    }
                    
                    self.activityindicator.stopAnimating()
                    sender.enabled=true
            }
            
        }
        sender.enabled=true
    }
    
}