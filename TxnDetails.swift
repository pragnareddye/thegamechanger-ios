//
//  TxnDetails.swift
//  The Game Changer
//
//  Created by Apple on 15/03/16.
//  Copyright © 2016 TheGameChanger. All rights reserved.
//

import Foundation
import UIKit




class TxnDetails: UIViewController {
    
    @IBOutlet weak var Location: UILabel!
    @IBOutlet weak var groundSize: UILabel!
    @IBOutlet weak var game: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var transactionID: UILabel!
    @IBOutlet weak var ticketID: UILabel!
    @IBOutlet weak var total: UILabel!
    @IBOutlet weak var subtotal: UILabel!
    @IBOutlet weak var discount: UILabel!
    @IBOutlet var ActivityIndicator: UIActivityIndicatorView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ActivityIndicator.startAnimating()
        fetchTransactionDetails();
    }
    
    
    @IBAction func backToBooking(sender: UIButton) {
        
        self.performSegueWithIdentifier("backToBooking", sender: self)
        
    }
    func fetchTransactionDetails()
    {
        do{
            let data = DataObj.successString.dataUsingEncoding(NSUTF8StringEncoding)
            let json:NSDictionary = try (NSJSONSerialization.JSONObjectWithData(data!, options:NSJSONReadingOptions()) as? NSDictionary)!
            ActivityIndicator.stopAnimating()
            ActivityIndicator.alpha = 0
            Location.text=json.valueForKey("ground") as?  String
            groundSize.text=json.valueForKey("portions") as?  String
            game.text=json.valueForKey("sport") as?  String
            date.text=json.valueForKey("days") as?  String
            time.text=json.valueForKey("timing") as?  String
            transactionID.text = "Transaction ID:\n" + (json.valueForKey("txnid") as!  String)
            ticketID.text = "Ticket ID:" + (json.valueForKey("ticket_id") as!  String)
            print(json.valueForKey("total") as! String)
            print(json.valueForKey("subtotal") as! String)
            let totalAmt = (json.valueForKey("total") as! NSString).floatValue
            let subTotalAmt = (json.valueForKey("subtotal") as! NSString).floatValue
            total.text = "Rs. \(totalAmt)"
            subtotal.text = "Rs. \(subTotalAmt)"
            let discountAmt = totalAmt - subTotalAmt
            discount.text = "Rs. \(discountAmt)"
        }
        catch
        {
            
        }
        
    }
    
}
