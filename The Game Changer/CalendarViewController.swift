//
//  CalendarViewController.swift
//  The Game Changer
//
//  Created by swaroop on 10/12/15.
//  Copyright (c) 2015 TheGameChanger. All rights reserved.
//

import Foundation
import UIKit

class CalendarViewController:UIViewController {
    override func viewDidLoad() {
        self.navigationItem.prompt = "Pick your date(s)"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
