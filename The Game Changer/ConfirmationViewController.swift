//
//  conformation.swift
//  The Game Changer
//
//  Created by local admin on 20/11/15.
//  Copyright (c) 2015 TheGameChanger. All rights reserved.
//


import UIKit
import Foundation

class ConfirmationViewController: UIViewController {
    
    @IBOutlet var gamename: UILabel!
    @IBOutlet weak var ActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var location: UILabel!
    @IBOutlet var gamemode: UILabel!
    @IBOutlet var datestring: UILabel!
    @IBOutlet var timestring: UILabel!
    @IBOutlet var price: UILabel!
    @IBOutlet var finalCost: UILabel!
    @IBOutlet var discountLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.navigationItem.hidesBackButton=true
        ActivityIndicator.startAnimating()
        dispatch_async(dispatch_get_main_queue()) {
            self.doSomething()
            self.ActivityIndicator.stopAnimating()
        }
    }
    func doSomething()
    {
        
        self.navigationController?.navigationBar.hidden = false
        var datesf:String = ""
        if(DataClass.Bulk==0)
        {
            datesf = DataClass.SelectedDateString
        }
        else
        {
            datesf=DataClass.SelectedDatesString
        }
        
        
        let post="sport=\(DataObj.SportId)&ground=\(DataObj.SelectedGround)&portions=\(DataObj.PortionsSelected)&days=\(datesf)&timing=\(DataObj.SlotsPicked)"
        NSLog("PostData: %@",post);
        let addr:NSString="reserve"
        
        let urlData: NSData? = DataObj.JSONFetch(post as String, Address: addr as String)
        
        
        if ( urlData != nil ) {
            
            do{
                let jsonData:NSDictionary = try NSJSONSerialization.JSONObjectWithData(urlData!, options:NSJSONReadingOptions.MutableContainers)  as! NSDictionary
                
                NSLog("Login SUCCESS");
                
                self.location.text=jsonData.valueForKey("ground") as?  String
                self.gamename.text=jsonData.valueForKey("sport") as?  String
                self.gamemode.text=jsonData.valueForKey("portions") as?  String
                self.datestring.text=jsonData.valueForKey("days") as?  String
                self.timestring.text=jsonData.valueForKey("timing") as? String
                let cost=jsonData.valueForKey("subtotal") as! NSInteger
                DataObj.TicketId = jsonData.valueForKey("ticket_id") as! String
                self.price.text="Rs. \(cost)"
                
                self.finalCost.text="Rs. \(cost)"
            }
            catch{
            }
            
            //self.dismissViewControllerAnimated(true, completion: nil)
            
        }
            
        else
        {
            if(Internet==0)
            {
                self.navigationController?.popViewControllerAnimated(true)
                let alertView:UIAlertView = UIAlertView()
                alertView.title = "Fetch Failed"
                alertView.message = "Check your internet Connection"
                
                alertView.delegate = self
                alertView.addButtonWithTitle("OK")
                alertView.show()
            }
        }
        
    }
    
    @IBOutlet var coupon: UITextField!
    
    @IBAction func ProceedToPayment(sender: UIButton) {
        dispatch_async(dispatch_get_main_queue()) {
            self.performSegueWithIdentifier("WebView", sender: self)
        }
        
    }
    @IBAction func apply_coupon(sender: UIButton) {
        let coupon1 :NSString=coupon.text!
        
        let post="coupon=\(coupon1)&ticket_id=\(DataObj.TicketId)"
        NSLog("PostData: %@",post);
        
        let addr:NSString="coupon"
        // DataObj = DataClass()
        
        let urlData: NSData? = DataObj.JSONFetch(post as String, Address: addr as String)
        
        if ( urlData != nil ) {
            
            do{
                let jsonData:NSDictionary = try NSJSONSerialization.JSONObjectWithData(urlData!, options:NSJSONReadingOptions.MutableContainers)  as! NSDictionary
                // let success:NSInteger=jsonData.valueForKey("success") as! NSInteger
                
                NSLog("Login SUCCESS");
                let discount :NSString=jsonData.valueForKey("discount") as! NSString
                discountLabel.text="\(discount)"
                //let subtotal:NSInteger?=100//finalCost.text?//.Int()
                //let finalcost:NSInteger=subtotal! - discount
                finalCost.text="Rs. \(jsonData.valueForKey("total") as! NSString)"
                
                
            }
                
            catch{
                
            }
            
        }
        else {
            navigationController?.popViewControllerAnimated(true)
            
            if(Internet==0)
            {
                let alertView:UIAlertView = UIAlertView()
                alertView.title = "Fetch failed!"
                alertView.message = "Connection Failure"
                alertView.delegate = self
                alertView.addButtonWithTitle("OK")
                alertView.show()
            }
            
        }
        
    }
    
    @IBAction func cancelBooking(sender: AnyObject) {
        let cancelAlert = UIAlertController(title: "Cancel Booking", message: "All data will be lost. Are you sure you want to cancel your booking?", preferredStyle: UIAlertControllerStyle.Alert)
        
        cancelAlert.addAction(UIAlertAction(title: "Yes", style: .Default, handler: { (action: UIAlertAction!) in
            print("Handle Ok logic here")
            dispatch_async(dispatch_get_main_queue()) {
                self.performSegueWithIdentifier("restartBooking", sender: self)
            }
            //self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
        }))
        
        cancelAlert.addAction(UIAlertAction(title: "No", style: .Default, handler: { (action: UIAlertAction!) in
            //Do nothing
        }))
        
        presentViewController(cancelAlert, animated: true, completion: nil)
    }
    
}
