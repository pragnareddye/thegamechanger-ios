
//
//  GroundsViewController.swift
//  The Game Changer
//
//  Created by local admin on 07/09/15.
//  Copyright (c) 2015 TheGameChanger. All rights reserved.
//

import Foundation
import UIKit



class GroundsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate{
    
    var flag:Bool = false
    //To store ground details
    class bask {
        var name :String = ""
        var GroundMinPortions :Int = 0
        
        var gid :Int = 0
        
        
        init(st :String,GroundMinPortions :Int  ,c: Int )
        {
            self.name = st
            self.GroundMinPortions = GroundMinPortions
            self.gid = c
            
        }
    }
    
    var arr = [bask]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DataClass.InternetFCS=0
        LoadView()
    }
    
    
    
    func LoadView()
    {
        DataClass.SelectedDatesArray=NSMutableArray()
        DataClass.AvailableDates=NSMutableArray()
        DataClass.SelectedDateString=""
        
        navigationItem.prompt = "Select your preferred ground"
        let post:NSString = "sport=\(DataObj.SportId)"
        
        let urlData: NSData? = DataObj.JSONFetch(post as String, Address:"grounds" as String)
        if(urlData != nil)
        {
            
            
            
            do{
                let rateDictionary:NSArray = try NSJSONSerialization.JSONObjectWithData(urlData!, options: NSJSONReadingOptions.MutableContainers ) as! NSArray
                
                for var ind = 0 ;ind < rateDictionary.count; ind++
                {
                    let GDetails:NSDictionary = rateDictionary[ind] as! NSDictionary
                    let name: String = GDetails.valueForKey("ground") as! String
                    let GroundMinPortionsv:Int = (GDetails.valueForKey("min_portions") as! NSString).integerValue
                    let gidv:Int  = GDetails.valueForKey("id") as!  Int
                    self.arr.append(bask(st : name , GroundMinPortions :GroundMinPortionsv , c:gidv  ))
                    
                }
            }
                
            catch{
            }
            
        }
        else
        {
            
            if(Internet==0 && DataClass.InternetFCS<10)
            {
                
                DataClass.InternetFCS=DataClass.InternetFCS+1;
                LoadView()
                
            }
            else
            {
                navigationController?.popViewControllerAnimated(true)
                let alertView:UIAlertView = UIAlertView()
                alertView.title = "Fetch Failed"
                alertView.message = "Check your internet Connection"
                
                alertView.delegate = self
                alertView.addButtonWithTitle("OK")
                alertView.show()
                
            }
            
            
        }
        self.flag=true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
    
    var selectedCell:UITableViewCell! = nil
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arr.count
    }
    
    //populating the table
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        var cell: UITableViewCell!
        cell = tableView.dequeueReusableCellWithIdentifier("GroundsCell")! as UITableViewCell
        cell.textLabel!.text = arr[indexPath.item].name
        return cell
        
    }
    
    //on selection of a ground
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        print(DataObj.SportMin)
        let cell:UITableViewCell = tableView.cellForRowAtIndexPath(indexPath)!
        cell.selectionStyle = UITableViewCellSelectionStyle.Blue
        let gid = arr[indexPath.item].gid
        DataObj.SelectedGround=gid
        let myActionSheet =  UIAlertController(title: "Select the ground size", message: "", preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        let portions:Int = max(DataObj.SportMin , arr[indexPath.item].GroundMinPortions)
        if (portions<=4)
        {
            myActionSheet.addAction(UIAlertAction(title: "5-a-side", style: UIAlertActionStyle.Default, handler: { (ACTION :UIAlertAction)in
                
                DataObj.PortionsSelected=4
                dispatch_async(dispatch_get_main_queue()) {
                    
                    if(DataClass.Bulk == 0){
                        
                        self.performSegueWithIdentifier("calendar", sender: self)
                    }
                    else
                    {
                        
                        self.performSegueWithIdentifier("slots_picker1", sender: self)
                    }
                }
            }))
        }
        if ( portions<=8)
        {
            
            myActionSheet.addAction(UIAlertAction(title: "7-a-side", style: UIAlertActionStyle.Default, handler: { (ACTION :UIAlertAction)in
                
                DataObj.PortionsSelected=8
                dispatch_async(dispatch_get_main_queue()) {
                    if(DataClass.Bulk == 0){
                        
                        self.performSegueWithIdentifier("calendar", sender: self)
                    }
                    else
                    {
                        
                        self.performSegueWithIdentifier("slots_picker1", sender: self)
                    }
                }
                
            }))
        }
        
        if(portions<=16)
        {
            myActionSheet.addAction(UIAlertAction(title: "Full ground", style: UIAlertActionStyle.Default, handler: { (ACTION :UIAlertAction)in
                
                DataObj.PortionsSelected=16
                DataObj.GroundId=self.arr[indexPath.item].gid
                dispatch_async(dispatch_get_main_queue()) {
                    if(DataClass.Bulk == 0){
                        self.performSegueWithIdentifier("calendar", sender: self)
                    }
                    else
                    {
                        
                        self.performSegueWithIdentifier("slots_picker1", sender: self)
                    }
                }        }))
        }
        
        
        myActionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil))
        
        self.presentViewController(myActionSheet, animated: true, completion: nil)
        
    }
    
    
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        let cell:UITableViewCell = tableView.cellForRowAtIndexPath(indexPath)!
        cell.accessoryType = UITableViewCellAccessoryType.None
    }
}





