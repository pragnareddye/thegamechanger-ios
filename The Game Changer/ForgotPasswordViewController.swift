//
//  ForgotPasswordViewController.swift
//  The Game Changer
//
//  Created by local admin on 03/09/15.
//  Copyright (c) 2015 TheGameChanger. All rights reserved.
//

import Foundation
import UIKit

class ForgotPasswordViewController: UIViewController {
 
    @IBOutlet weak var ActivityIndicator: UIActivityIndicatorView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.navigationController?.navigationBar.hidden = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    @IBOutlet var email: UITextField!
    
    @IBAction func resetpassword(sender: UIButton) {
        ActivityIndicator.startAnimating()
        sender.enabled=false
        dispatch_async(dispatch_get_main_queue()) {
        let email1:NSString=self.email.text!
        NSLog(email1 as String)
        if(email1.isEqualToString(""))
        {
            let alertView:UIAlertView = UIAlertView()
            alertView.title = "Failed!"
            alertView.message = "Please enter email"
            alertView.delegate = self
            alertView.addButtonWithTitle("OK")
            alertView.show()
            
        }
        else
        {
            let post:NSString = "email=\(email1)"
            
            NSLog("PostData: %@",post);
            let addr:NSString="forgotpassword"
            
            
            let urlData: NSData? = DataObj.JSONFetch(post as String, Address: addr as String)
            if(urlData != nil)
            {
                self.navigationController?.popViewControllerAnimated(true)
                let alertView:UIAlertView = UIAlertView()
                alertView.title = "Success!"
                alertView.message = "Email has been Sent. Login"
                alertView.delegate = self
                alertView.addButtonWithTitle("OK")
                alertView.show()

                
            }
            else
            {
                
                if(Internet==0)
                {
                    let alertView:UIAlertView = UIAlertView()
                    alertView.title = "Error!"
                    alertView.message = "Check your internet connection"
                    alertView.delegate = self
                    alertView.addButtonWithTitle("OK")
                    alertView.show()
                }
                else
                {
                    let alertView:UIAlertView = UIAlertView()
                    alertView.title = "Error!"
                    alertView.message = "Try again"
                    alertView.delegate = self
                    alertView.addButtonWithTitle("OK")
                    alertView.show()
                }
            }
            
            
        }
            
            self.ActivityIndicator.stopAnimating()
}
        sender.enabled=true
    }
}