//
//  LoginViewController.swift
//  The Game Changer
//
//  Created by local admin on 21/08/15.
//  Copyright (c) 2015 TheGameChanger. All rights reserved.
//

import Foundation
import UIKit


var prefs:NSUserDefaults = NSUserDefaults.standardUserDefaults()
var DataObj:DataClass = DataClass.getObject();

class LoginViewController: UIViewController {
    @IBOutlet weak var ActivityIndicator: UIActivityIndicatorView!
    var InternetFetchCount:Int=0;
    @IBOutlet var Username: UITextField!
    @IBOutlet var Password: UITextField!
    var username11:NSString=""
    var password11:NSString=""
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        InternetFetchCount=0;
    }
    
    override func viewWillAppear(animated: Bool)
    {
        self.navigationController?.navigationBar.hidden = true
        Username.text=""
        Password.text=""
        
        
        if (prefs.objectForKey("UserName") != nil && prefs.objectForKey("password") != nil) {
            print("sdfgh");
            
            username11 = prefs.objectForKey("UserName") as! NSString
            password11 = prefs.objectForKey("password") as! NSString
            
            if (DataClass.sendsinfor(username11, pass:password11)){
                dispatch_async(dispatch_get_main_queue()) {
                    self.performSegueWithIdentifier("login_1", sender: self)
                }
            }
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
    
    @IBAction func login_tapped(sender: UIButton) {
        let username=Username.text!
        let pass=Password.text!
        if(username=="" || pass=="")
        {
            let alertView:UIAlertView = UIAlertView()
            alertView.title = "Error!"
            alertView.message = "Enter your details1"
            alertView.delegate = self
            alertView.addButtonWithTitle("OK")
            alertView.show()
        }
        else
        {
            ActivityIndicator.startAnimating()
            dispatch_async(dispatch_get_main_queue()) {
                
                if (DataClass.sendsinfor(username, pass:pass)){
                    
                    self.performSegueWithIdentifier("login_1", sender: self)
                }
                self.ActivityIndicator.stopAnimating()
                
            }
            
        }
        
    }
    @IBAction func forgotPass_tapped(sender: UIButton) {
        dispatch_async(dispatch_get_main_queue()) {
            self.performSegueWithIdentifier("forgot_pass", sender: self)
            
        }
        
    }
    
    @IBAction func signup(sender: UIButton) {
        dispatch_async(dispatch_get_main_queue()) {
            self.performSegueWithIdentifier("signup", sender: self)
        }
    }
    
    
    
    
    
}