
#import <UIKit/UIKit.h>

@interface KDCalendarViewDayCell : UICollectionViewCell
{
    UIView* _selectedMarkView;
    UIView* _todayMarkView;
    UIView* _eventsMarksView;
}

@property (nonatomic, strong) UILabel* label;
@property (nonatomic) BOOL isToday;
@property (nonatomic) BOOL isCurrentMonth;
@property (nonatomic) BOOL isDaySelected;
@property (nonatomic, strong) NSMutableArray* myArray;
@property (nonatomic, weak) NSArray* events;

@property (nonatomic, strong) NSDate* date;


@end
