
import Foundation
import UIKit

public class WebView: UIViewController {
    
    
    
    @IBOutlet var ActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var myWebView: UIWebView!
    override public func viewDidLoad() {
        super.viewDidLoad()
        ActivityIndicator.startAnimating()
        dispatch_async(dispatch_get_main_queue()) {
            self.loadpage()
            self.ActivityIndicator.stopAnimating()
        }
        
    }
    
    func loadpage()
    {
        ActivityIndicator.startAnimating()
        let post:NSString = "ticket_id=\(DataObj.TicketId)"
        let addr:NSString="pay"
        let urlData: NSData? = DataObj.JSONFetch(post as String, Address: addr as String)
        if(urlData != nil)
        {
            let responseData:NSString  = NSString(data:urlData!, encoding:NSUTF8StringEncoding)!
            NSLog(responseData as String)
            
            do{
                let jsonData:NSDictionary = try NSJSONSerialization.JSONObjectWithData(urlData!, options:NSJSONReadingOptions.MutableContainers)  as! NSDictionary
                let payment:NSDictionary =  jsonData.valueForKey("payment") as! NSDictionary
                DataClass.token=jsonData.valueForKey("token") as! NSString
                let postDataR:NSDictionary =  payment.valueForKey("post_data") as! NSDictionary
                let action :NSString = payment.valueForKey("action") as! NSString
                let Parameters:String = postDataR.description
                print(Parameters)
                
                
                let a:NSString =    postDataR.valueForKey("amount") as! NSString
                let b:NSString = postDataR.valueForKey("email") as! NSString
                let c:NSString = postDataR.valueForKey("firstname") as! NSString
                let d:NSString = postDataR.valueForKey("furl") as! NSString
                let e:NSString = postDataR.valueForKey("hash") as! NSString
                let f:NSString = postDataR.valueForKey("key") as! NSString
                let g:NSString = postDataR.valueForKey("phone") as! NSString
                let h:NSString = postDataR.valueForKey("productinfo") as! NSString
                let sp:NSString = postDataR.valueForKey("service_provider") as! NSString
                let i:NSString =  postDataR.valueForKey("surl") as! NSString
                let j:NSString = postDataR.valueForKey("txnid") as! NSString
                let k:NSString = postDataR.valueForKey("udf1") as! NSString
                let l:NSString = postDataR.valueForKey("udf2") as! NSString
                let m:NSString = postDataR.valueForKey("udf3") as! NSString
                let n:NSString = postDataR.valueForKey("udf4") as! NSString
                let o:NSString = postDataR.valueForKey("udf5") as! NSString
                
                
                let postData:NSString="amount=\(a)&email=\(b)&firstname=\(c)&furl=\(d)&hash=\(e)&key=\(f)&phone=\(g)&productinfo=\(h)&service_provider=\(sp)&surl=\(i)&txnid=\(j)&udf1=\(k)&udf2=\(l)&udf3=\(m)&udf4=\(n)&udf5=\(o)"
                let escapedString:NSString = postData.stringByAddingPercentEncodingWithAllowedCharacters(.URLHostAllowedCharacterSet())!
                print(action)
                print(escapedString )
                
                
                //internet
                
                let url = NSURL (string: "\(action)")
                let request = NSMutableURLRequest(URL: url!)
                request.HTTPMethod = "POST"
                request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
                let post: String = "\(escapedString)"
                let postData1: NSData = post.dataUsingEncoding(NSASCIIStringEncoding, allowLossyConversion: true)!
                request.HTTPBody = postData1
                myWebView.loadRequest(request)
                //myWebView.loadRequest(NSURL("http://api.thegamechanger.club/api/success"))
                //internet
                
                
                
            }
            catch{
                
                
            };
            
        }
        ActivityIndicator.hidesWhenStopped=true;
        ActivityIndicator.stopAnimating()
        ActivityIndicator.hidden = false
        
        
    }
    func webViewDidStartLoad(webView: UIWebView){
        
        ActivityIndicator.startAnimating()
        
        
    }
    func webViewDidFinishLoad(webView: UIWebView){
        
        ActivityIndicator.hidden = true
        ActivityIndicator.stopAnimating()
        let UrlLoaded : NSString = (webView.request?.mainDocumentURL?.absoluteString)!
        if(UrlLoaded=="http://api.thegamechanger.club/api/success")
        {
            let pageTitle:NSString = myWebView.stringByEvaluatingJavaScriptFromString("document.title")! as NSString
            DataObj.successString = pageTitle
            // let gh:NSDictionary = pageTitle as NSDictionary
            NSLog("Requested url: %@", pageTitle);
            dispatch_async(dispatch_get_main_queue()) {
                self.performSegueWithIdentifier("TxnDetails", sender: self)
            }
            
        }
        else if(UrlLoaded=="http://api.thegamechanger.club/api/fail")
        {
            
            let cancelAlert = UIAlertController(title: "Payment Failed", message: "Try again", preferredStyle: UIAlertControllerStyle.Alert)
            
            cancelAlert.addAction(UIAlertAction(title: "ok", style: .Default, handler: { (action: UIAlertAction!) in
                print("Handle Ok logic here")
                dispatch_async(dispatch_get_main_queue()) {
                    self.performSegueWithIdentifier("restartBookingWeb", sender: self)
                }
                //self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
            }))
            
            
            presentViewController(cancelAlert, animated: true, completion: nil)
        }
        
        
    }
    
    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func cancelTransaction(sender: AnyObject) {
        let cancelAlert = UIAlertController(title: "Cancel Booking", message: "Your slots have already been blocked. Are you sure you want to cancel your booking?", preferredStyle: UIAlertControllerStyle.Alert)
        
        cancelAlert.addAction(UIAlertAction(title: "Yes", style: .Default, handler: { (action: UIAlertAction!) in
            print("Handle Ok logic here")
            dispatch_async(dispatch_get_main_queue()) {
                self.performSegueWithIdentifier("restartBookingWeb", sender: self)
            }
            //self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
        }))
        
        cancelAlert.addAction(UIAlertAction(title: "No", style: .Default, handler: { (action: UIAlertAction!) in
            //Do nothing
        }))
        
        presentViewController(cancelAlert, animated: true, completion: nil)
    }
    
}
