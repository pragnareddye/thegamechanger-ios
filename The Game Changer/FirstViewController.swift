//
//  FirstViewController.swift
//  The Game Changer
//
//  Created by local admin on 18/08/15.
//  Copyright (c) 2015 TheGameChanger. All rights reserved.
//

import UIKit
public class FirstViewController: UIViewController {
    
    @IBOutlet var btnFootball: UIButton!
    @IBOutlet var btnVolleyball: UIButton!
    @IBOutlet var btnFrisbee: UIButton!
    @IBOutlet var btnRugby: UIButton!
    @IBOutlet var bookingTypeHint: UILabel!
    
    var colorBlue:UIColor = UIColor(red: 38/255, green: 101/255, blue: 189/255, alpha: 1)
    var colorGreen:UIColor = UIColor(red: 56/255, green: 173/255, blue: 24/255, alpha: 1)
    var gameImage: String = " "
    var game:Int=0;
    var bulk:Int = 0;
    override public func viewDidLoad() {
        super.viewDidLoad()
       
        // Do any additional setup after loading the view, typically from a nib.
        self.navigationController?.navigationBar.hidden = false
        //self.navigationItem.hidesBackButton=true

    }
    
    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func bulk_value(sender: UISegmentedControl) {
        if(bulk == 0)
        {
            bulk = 1
        }
        else
        {
            bulk = 0
        }
        switch(sender.selectedSegmentIndex)
        {
        case 0: bookingTypeHint.text = "To book only one date"
            bulk = 0
            break
        case 1: bookingTypeHint.text = "To book multiple dates at the same time"
            break
        default: bookingTypeHint.text = ""
            break
        }
    }
    @IBAction func sportsFunction(sender: UIButton){
        let previous_selected_game: Int
        print(sender.tag)
        previous_selected_game = game
        game = sender.tag
        if (game != previous_selected_game)
        {
            getButton(previous_selected_game).alpha = 1
            sender.alpha = 0.5
        }
        
    }
    
    @IBAction func seg_control(sender: UISegmentedControl) {
        
    }
    
    @IBAction func lets_go(sender: UIButton) {
        if(game > 0)
        {
            dispatch_async(dispatch_get_main_queue())
                {
                    DataObj.SportId=self.game
                    DataClass.Bulk=self.bulk
                    self.performSegueWithIdentifier("grounds_picker", sender: self)
                }
        }
        else
        {
            let alertView:UIAlertView = UIAlertView()
            alertView.title = "Select a game"
            alertView.message = ""
            alertView.delegate = self
            alertView.addButtonWithTitle("OK")
            alertView.show()
        }
        
    }
    
    func getButton(game:Int)->UIButton
    {
        switch(game)
        {
        case 1: gameImage = "btnFootballImage"
            print("football")
            DataObj.SportMin=4
            DataObj.SportId=1
            return btnFootball
        case 2: gameImage = "btnVolleyballImage"
            print("volleyball")
            DataObj.SportMin=4
            DataObj.SportId=2
            return btnVolleyball
        case 4: gameImage = "btnFrisbeeImage"
            print("frisbee")
            DataObj.SportMin=16
            DataObj.SportId=4
            return btnFrisbee
        default: gameImage = "btnRugbyImage"
            print("rugbyball")
            DataObj.SportMin=16
            DataObj.SportId=8
            return btnRugby
        }
    }
}

