

#import "KDCalendarLineReusableView.h"

@implementation KDCalendarLineReusableView

-(void)applyLayoutAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes
{
    [super applyLayoutAttributes:layoutAttributes];
    
    self.backgroundColor = [UIColor colorWithWhite:0.2 alpha:0.3];

    
}

@end
