
//
//  TimeViewController.swift
//  The Game Changer
//
//  Created by swaroop on 10/6/15.
//  Copyright (c) 2015 TheGameChanger. All rights reserved.
//
import UIKit
import Foundation



class TimeViewController: UIViewController {
    var colorBlue:UIColor = UIColor(red: 38/255, green: 101/255, blue: 189/255, alpha: 1)
    var colorGreen:UIColor = UIColor(red: 56/255, green: 173/255, blue: 24/255, alpha: 1)
    var colorGreen2:UIColor = UIColor(red: 56/255, green: 220/255, blue: 24/255, alpha: 1)
    
    @IBOutlet weak var ActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var NoSlots: UILabel!
    var x = 8
    var y = 8
    var count:Int=0
    var total:UInt64=0
    var costEstimate:Float=0
    @IBOutlet var myscrollview: UIScrollView!
    @IBOutlet var timeSlotsView: UIView!
    @IBOutlet var costEstimateLabel: UILabel!
    @IBOutlet var activityindicator: UIActivityIndicatorView!
    @IBOutlet var costEstimatePrompt: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.NoSlots.alpha=0;
        self.costEstimateLabel.alpha = 0
        self.costEstimatePrompt.alpha = 0
        ActivityIndicator.startAnimating()
        dispatch_async(dispatch_get_main_queue()) {
            self.doSome()
        }
        
        
    }
    
    func doSome()
    {
        
        
        self.navigationItem.prompt="Pick your time slot(s)"
        
        
        let inFormatter = NSDateFormatter()
        inFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
        inFormatter.dateFormat = "HH:mm"
        count=0;
        total=0;
        
        if(DataClass.Bulk==0)
        {
            self.costEstimateLabel.alpha = 1
            self.costEstimatePrompt.alpha = 1
            self.costEstimateLabel.text="Rs. 0"
            
            
            let post:NSString = "ground=\(DataObj.SelectedGround)&date=\(DataClass.SelectedDateString)&portions=\(DataObj.PortionsSelected)"
            NSLog(post as String)
            let addr:NSString="times"
            let urlData: NSData? = DataObj.JSONFetch(post as String, Address: addr as String)
            if(urlData != nil)
            {
                let responseData:NSString  = NSString(data:urlData!, encoding:NSUTF8StringEncoding)!
                NSLog(responseData as String)
                
                do{
                    let jsonData:NSDictionary = try NSJSONSerialization.JSONObjectWithData(urlData!, options:NSJSONReadingOptions.MutableContainers)  as! NSDictionary
                    
                    let timing:UInt64=UInt64(jsonData.valueForKey("timing") as! String)!
                    let Booked:UInt64=UInt64(jsonData.valueForKey("booked") as! String)!
                    let Reserved:UInt64=UInt64(jsonData.valueForKey("reserved") as! String)!
                    var date = inFormatter.dateFromString("00:00")!
                    let cost =  jsonData.valueForKey("cost") as! NSDictionary;
                    DataObj.night=(cost.valueForKey("night") as! NSString).floatValue
                    DataObj.day=(cost.valueForKey("day") as! NSString).floatValue
                    
                    var count:Int = 0;
                    for var i:UInt64=0;i<48;i++
                    {
                        let bit_timing = (timing>>i)&1
                        let bit_Booked = (Booked>>i)&1
                        let bit_Reserved =  (Reserved>>i)&1
                        
                        if(bit_timing==1)
                        {
                            let outStr  : String = inFormatter.stringFromDate(date)
                            print(outStr)
                            self.addButton("\(outStr)", index: i, flag1:bit_Booked,flag2:bit_Reserved,tag:count)
                            
                        }
                        date = date.dateByAddingTimeInterval(30*60)
                        
                        count++
                        
                    }
                    if(timing == 0)
                    {
                        self.NoSlots.alpha=1;
                        
                    }
                    else
                    {
                        self.NoSlots.alpha=0;
                    }
                    
                    
                }
                catch{
                    
                    
                };
                
            }
            else
            {
                if(Internet==0 && DataClass.InternetFCS<10)
                {
                    
                    DataClass.InternetFCS=DataClass.InternetFCS+1;
                    self.doSome()
                    
                }
                else
                {
                    self.navigationController?.popViewControllerAnimated(true)
                    let alertView:UIAlertView = UIAlertView()
                    alertView.title = "Fetch Failed"
                    alertView.message = "Check your internet Connection"
                    
                    alertView.delegate = self
                    alertView.addButtonWithTitle("OK")
                    alertView.show()
                    
                }
            }
            
        }
            
            //*********if it is bulk booking
            
        else
        {
            
            
            let post:NSString = "ground=\(DataObj.SelectedGround)"
            
            let addr:NSString="times"
            
            
            let urlData: NSData? = DataObj.JSONFetch(post as String, Address: addr as String)
            if(urlData != nil)
            {
                let responseData:NSString  = NSString(data:urlData!, encoding:NSUTF8StringEncoding)!
                NSLog(responseData as String)
                
                
                
                
                do{
                    let jsonData:NSDictionary = try NSJSONSerialization.JSONObjectWithData(urlData!, options:NSJSONReadingOptions.MutableContainers)  as! NSDictionary
                    
                    let timing:UInt64=UInt64(jsonData.valueForKey("timing") as! String)!
                    
                    var date = inFormatter.dateFromString("00:00")!
                    
                    var count:Int = 0;
                    for var i:UInt64 = 0;i<48;i++
                    {
                        let bit_timing = (timing>>i)&1
                        
                        if(bit_timing==1)
                        {
                            let outStr  : String = inFormatter.stringFromDate(date)
                            self.addButton("\(outStr)", index: i, flag1:0,flag2:0,tag:count)
                            
                        }
                        date = date.dateByAddingTimeInterval(30*60)
                        
                        count++;
                        
                    }
                    
                    
                }
                catch{
                    
                    
                };
                
            }
            else
            {
                if(Internet==0 && DataClass.InternetFCS<10)
                {
                    
                    DataClass.InternetFCS=DataClass.InternetFCS+1;
                    doSome()
                    
                }
                else
                {
                    navigationController?.popViewControllerAnimated(true)
                    let alertView:UIAlertView = UIAlertView()
                    alertView.title = "Fetch Failed"
                    alertView.message = "Check your internet Connection"
                    
                    alertView.delegate = self
                    alertView.addButtonWithTitle("OK")
                    alertView.show()
                    
                }
                
                
            }
            
            
            
            
        }
        ActivityIndicator.stopAnimating()
        
        
    }
    
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated);
        
    }
    func buttonAction(sender:UIButton!)
    {
        
        let inFormatter = NSDateFormatter()
        inFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
        inFormatter.dateFormat = "HH:mm"
        let Bdate:NSDate=inFormatter.dateFromString(sender.titleLabel?.text as String!)!
        let Adate:NSDate=inFormatter.dateFromString("6:00")!
        let Pdate:NSDate=inFormatter.dateFromString("18:00")!
        let dateComparisionResult: NSComparisonResult = Bdate.compare(Adate)
        let datecomp:NSComparisonResult = Bdate.compare(Pdate)
        var cost:Float=DataObj.night
        if ((dateComparisionResult == NSComparisonResult.OrderedDescending || dateComparisionResult == NSComparisonResult.OrderedSame) && datecomp == NSComparisonResult.OrderedAscending)
        {
            
            cost=DataObj.day
            
        }
        var mask :UInt64 = 0
        if sender.backgroundColor == colorGreen
        {
            sender.backgroundColor = colorBlue
            let val :UInt64 = UInt64(sender.tag)
            
            count++
            mask = 1<<val
            total = total | mask
            
            costEstimate+=cost
            
        }
        else
        {
            sender.backgroundColor = colorGreen
            let val :UInt64 = UInt64(sender.tag)
            
            count--
            mask = 1<<val
            total = total & mask
            costEstimate-=cost
            
        }
        print("hg")
        print(total)
        costEstimateLabel.text="Rs. \(costEstimate)"
    }
    
    @IBAction func next(sender: UIButton) {
        
        
        if(count>=2)
        {
            DataObj.SlotsPicked=total
            if(DataClass.Bulk==0){
                let refreshAlert = UIAlertController(title: "Confirm", message: "Are you want to proceed? Your time slots will be blocked.", preferredStyle: UIAlertControllerStyle.Alert)
                
                refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
                    self.performSegueWithIdentifier("con1", sender: self)
                }))
                
                refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { (action: UIAlertAction!) in
                }))
                
                presentViewController(refreshAlert, animated: true, completion: nil)
                
            }
            else
            {
                let post:NSString = "ground=\(DataObj.SelectedGround)&portions=\(DataObj.PortionsSelected)&timing=\(total)"
                
                let addr:NSString="days"
                
                
                let urlData: NSData? = DataObj.JSONFetch(post as String, Address: addr as String)
                if(urlData != nil)
                {
                    let responseData:NSString  = NSString(data:urlData!, encoding:NSUTF8StringEncoding)!
                    NSLog(responseData as String)
                    do{
                        let jsonData:NSDictionary = try NSJSONSerialization.JSONObjectWithData(urlData!, options:NSJSONReadingOptions.MutableContainers)  as! NSDictionary
                        
                        
                        DataClass.AvailableDates=jsonData.valueForKey("days") as! NSMutableArray
                        
                    }
                    catch{
                        
                        
                    };
                    
                }
                else
                {
                    
                    let alertView:UIAlertView = UIAlertView()
                    alertView.title = "Fetch Failed"
                    alertView.message = "Check your internet Connection"
                    
                    alertView.delegate = self
                    alertView.addButtonWithTitle("OK")
                    alertView.show()
                    
                    
                }
                
                
                self.performSegueWithIdentifier("date_picker2", sender: self)
            }
        }
        else{
            let alertView:UIAlertView = UIAlertView()
            alertView.title = "Error!"
            alertView.message = "Pick atleast 2 slots"
            alertView.delegate = self
            alertView.addButtonWithTitle("OK")
            alertView.show()
        }
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addButton(buttonTime: String!, index: UInt64 , flag1:UInt64,flag2:UInt64,tag:Int) {
        let button = UIButton(type: UIButtonType.System) as UIButton
        button.backgroundColor = self.colorGreen
        if(flag1==1)
        {
            button.enabled = false
            button.backgroundColor = UIColor.redColor()  //self.colorGreen2
            
        }
        else if(flag2==1)
        {
            button.enabled = false
            button.backgroundColor = UIColor.orangeColor()
        }
        button.frame = CGRectMake(CGFloat(x), CGFloat(y),50,50)
        button.layer.cornerRadius = 2
        button.tag=tag
        button.setTitle(buttonTime, forState: UIControlState.Normal)
        button.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        button.addTarget(self, action: "buttonAction:", forControlEvents: UIControlEvents.TouchUpInside)
        self.timeSlotsView.addSubview(button)
        if (index+1)%6 == 0
        {
            y = 8
            x = x + 55
        }
        else
        {
            y = y + 55
        }
        
    }
}





