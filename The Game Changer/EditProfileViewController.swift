//
//  EditProfileViewController.swift
//  The Game Changer
//
//  Created by local admin on 9/26/15.
//  Copyright (c) 2015 TheGameChanger. All rights reserved.
//


import Foundation
import UIKit

class EditProfileViewController: UIViewController, UINavigationControllerDelegate {
    
    @IBOutlet weak var activityindicator: UIActivityIndicatorView!
    @IBOutlet var firstName: UITextField!
    @IBOutlet var lastName: UITextField!
    @IBOutlet var mobileNumber: UITextField!
    @IBOutlet var oldPassword: UITextField!
    @IBOutlet var newPassword: UITextField!
    @IBOutlet var DatePicker: UIDatePicker!
    
    var fname:String = ""
    var lname:String=""
    var mobile:String=""
    var newpass:String=""
    var oldpass:String=""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        navigationItem.prompt = "You can edit your details here"
        Loaddetails()
        DatePicker.setValue(UIColor.whiteColor(), forKey: "textColor")
        DatePicker.datePickerMode = .DateAndTime
        DatePicker.datePickerMode = .Date
        DatePicker.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)
    }
    
    
    func Loaddetails()
    {
        firstName.text=prefs.objectForKey("name") as? String
        lastName.text=prefs.objectForKey("nickname") as? String
        mobileNumber.text=prefs.objectForKey("mobile") as? String
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let stringDate = prefs.objectForKey("dob") as! String
        DatePicker.setDate(dateFormatter.dateFromString(stringDate)!, animated: true)
    }
    
    
    
    @IBAction func saveChanges(sender: UIButton) {
        activityindicator.startAnimating();
        activityindicator.sizeToFit()
        
        
        dispatch_async(dispatch_get_main_queue())
            {
                
                sender.enabled=false
                var flag:Bool=true
                let firstName1:String = self.firstName.text!
                let lastName1:String = self.lastName.text!
                let mobileNumber1:String  = self.mobileNumber.text!
                
                let password1: String = self.oldPassword.text!
                if firstName1 == "" || lastName1 == "" || mobileNumber1 == ""  {
                    flag = false
                    let alertView:UIAlertView = UIAlertView()
                    alertView.title = "Edit Failed!"
                    alertView.message = "Please all the details"
                    alertView.delegate = self
                    alertView.addButtonWithTitle("OK")
                    alertView.show()
                    sender.enabled=true
                    
                }
                
                
                
                if(flag)
                    
                {
                    self.activityindicator.startAnimating();
                    self.activityindicator.sizeToFit()
                    
                    
                    dispatch_async(dispatch_get_main_queue())
                        {
                            
                            var post:NSString=""
                            if(self.newPassword.text != "")
                            {
                                post = "email=\(prefs.objectForKey("email") as! String)&name=\(firstName1)&nickname=\(lastName1)&mobile=\(mobileNumber1)&oldpassword=\(password1)&dob=\(prefs.objectForKey("dob") as! String)&password=\(self.newPassword.text)&confirm_password\(self.newPassword.text)"
                            }
                            else
                            {
                                post = "email=\(prefs.objectForKey("email") as! String)&name=\(firstName1)&nickname=\(lastName1)&mobile=\(mobileNumber1)&password=\(password1)&dob=\(prefs.objectForKey("dob") as! String)"
                            }
                            let addr:NSString="updateme"
                            let DataObj1:DataClass = DataClass();
                            let urlData: NSData? = DataObj1.JSONFetch(post as String, Address: addr as String)
                            if(urlData != nil)
                            {
                                let responseData:NSString  = NSString(data:urlData!, encoding:NSUTF8StringEncoding)!
                                NSLog(responseData as String)
                                let alertView:UIAlertView = UIAlertView()
                                alertView.title = "Success"
                                alertView.message = "Changes updated. Login to continue"
                                alertView.delegate = self
                                alertView.addButtonWithTitle("OK")
                                alertView.show()
                                prefs.removeObjectForKey("UserName")
                                prefs.removeObjectForKey("password")
                                self.dismissViewControllerAnimated(true, completion: nil)
                                
                            }
                            else
                            {
                                
                                if(Internet==0)
                                {
                                    let alertView:UIAlertView = UIAlertView()
                                    alertView.title = "Error!"
                                    alertView.message = "Check your internet connection"
                                    alertView.delegate = LoginViewController.self
                                    alertView.addButtonWithTitle("OK")
                                    alertView.show()
                                }
                                
                                
                            }
                            
                            self.activityindicator.stopAnimating()
                            sender.enabled=true
                    }
                    
                }
                
                
                sender.enabled=true
                self.activityindicator.stopAnimating()
                sender.enabled=true
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}



