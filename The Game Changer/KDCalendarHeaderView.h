
#import <UIKit/UIKit.h>

@interface KDCalendarHeaderView : UICollectionReusableView

+ (CGFloat) height;

@end
