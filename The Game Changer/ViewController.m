//
//  ViewController.m
//  KDCalendar
//
//  Created by Michael Michailidis on 14/02/2015.
//  Copyright (c) 2015 karmadust. All rights reserved.
//

#import "ViewController.h"
#import "KDCalendarView.h"
#import "The_Game_Changer-Swift.h"

@interface ViewController () <KDCalendarDelegate, KDCalendarDataSource, UITextFieldDelegate>


@property (nonatomic, weak) IBOutlet KDCalendarView* calendarView;

@property (nonatomic, strong) NSDateFormatter* formatter;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstaint;

@property (nonatomic, weak) IBOutlet UILabel* monthDisplayedDayLabel;

@property (nonatomic, weak) IBOutlet UITextField* inputTextField;

@property (nonatomic, weak) IBOutlet UIView* overlayView;

@property (nonatomic) CGFloat originalHeightOfConstaint;

@property (weak, nonatomic) IBOutlet UIView *bottomContainer;

@property (weak, nonatomic) IBOutlet UIButton *previousMonthButton;
@property (weak, nonatomic) IBOutlet UIButton *nextMonthButton;

@end
static  NSMutableArray *AD;
@implementation ViewController

- (IBAction)dateselected1:(UIButton *)sender {
    // dispatch_async(dispatch_get_main_queue(), ^(void){
    //Run UI Updates
    //int bulk = [[NSUserDefaults standardUserDefaults]  integerForKey:@"bulk"];
    NSLog(@"date%@",DataClass.SelectedDateString);
    NSLog(@"dates%@",DataClass.SelectedDatesString);
    if(DataClass.Bulk==0)
    {
        if([DataClass.SelectedDateString isEqual:@""])
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"ErrorS"
                                                            message:@"Pick a Date"
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
        else
        {
            [self performSegueWithIdentifier:@"slots_picker0" sender:self];
        }
        
    }
    else
    {
        if([DataClass.SelectedDatesString isEqual:@""])
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Errorm"
                                                            message:@"Pick a Date"
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];

        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                            message:@"...Do you want to proceed?"
                                                           delegate:self
                                                  cancelButtonTitle:@"No"
                                                  otherButtonTitles:@"Yes", nil];
            
           [alert show];
        }
        
    }
    
    
    
    //});
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch(buttonIndex) {
        case 0: //"No" pressed
            //do something?
            break;
        case 1: //"Yes" pressed
            //here you pop the viewController
            [self performSegueWithIdentifier:@"con2" sender:self];
            break;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //DataClass *dd  = [[DataClass alloc] init];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    //DataClass.SelectedDateString=@"";
    //DataClass.SelectedDateString=@"";
   // DataClass.SelectedDatesString=@"";
    DataClass.SelectedDatesArray=[[NSMutableArray alloc] init];
    NSMutableArray *mutablearray= [[NSMutableArray alloc] init];
    [userDefaults setObject:mutablearray forKey:@"selected_dates"];
    
    printf("bullk%ld",(long)DataClass.Bulk);
    
    self.formatter = [[NSDateFormatter alloc] init];
    [self.navigationItem setPrompt:@"Pick your date(s)"];
    /*dispatch_async(dispatch_get_main_queue(), ^(void){
     //Run UI Updates
     sleep(5);
     [self performSegueWithIdentifier:@"slots_picker0" sender:self];
     }); */
    
    //selectedPictures =  [[NSMutableArray alloc]init]; //initilise hear
    AD = [[NSMutableArray alloc]init];
    [AD addObject:@"CAT"];
    NSLog(@"%@",AD.description);
    self.formatter.dateFormat = @"yyyy-MM-dd";
    
    self.originalHeightOfConstaint = self.bottomConstaint.constant;
    
    self.calendarView.delegate = self;
    self.calendarView.dataSource = self;
    
    self.calendarView.showsEvents = YES;
    
    
    self.overlayView.userInteractionEnabled = NO;
    //self.monthDisplayedDayLabel.backgroundColor = [UIColor clearColor];
    //self.monthDisplayedDayLabel.textColor = [UIColor whiteColor];
    UITapGestureRecognizer* tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                           action:@selector(tapGestureOccured:)];
    
    [self.overlayView addGestureRecognizer:tapGestureRecognizer];
    
    [self.view bringSubviewToFront:self.overlayView]; // Originally placed in the back so as not to obstruct the storyboard outlets.
    [self.view bringSubviewToFront:self.bottomContainer];
    // NSMutableDictionary *selectedItemsInfo = [NSMutableDictionary new];
    
    self.overlayView.alpha = 0.0f;

    
}

- (void) tapGestureOccured:(UITapGestureRecognizer*)recognizer
{
    
    [self.inputTextField resignFirstResponder];
    
}




- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
   
}


- (void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    
    
}


- (void) keyboardFrameDidChange:(NSNotification*)notification
{
    
    
    
    
    self.overlayView.userInteractionEnabled = (BOOL)(notification.name == UIKeyboardWillShowNotification);
    
    UIViewAnimationCurve animationCurve = [[notification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    NSTimeInterval animationDuration = [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] integerValue];
    
    CGRect rawFrame = [[notification.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    CGRect keyboardFrame = [self.view convertRect:rawFrame fromView:nil];
    
    
    self.bottomConstaint.constant = (notification.name == UIKeyboardWillShowNotification) ? keyboardFrame.size.height : self.originalHeightOfConstaint;
    [self.view setNeedsUpdateConstraints];
    
    [UIView beginAnimations: nil context:nil];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:animationCurve];
    
    [self.view layoutIfNeeded];
    
    self.overlayView.alpha = (notification.name == UIKeyboardWillShowNotification) ? 1.0 : 0.0;
    
    [UIView commitAnimations];
}


#pragma mark - KDCalendarDataSource


-(NSDate*)startDate
{
    
    NSDateComponents *offsetDateComponents = [[NSDateComponents alloc] init];
    
    offsetDateComponents.month = 0;
    
    NSDate *threeMonthsBeforeDate = [[NSCalendar currentCalendar]dateByAddingComponents:offsetDateComponents
                                                                                 toDate:[NSDate date]
                                                                                options:0];
    
    
    return threeMonthsBeforeDate;
}


-(NSDate*)endDate
{
    NSDateComponents *offsetDateComponents = [[NSDateComponents alloc] init];
    //offsetDateComponents.year = 1;
    offsetDateComponents.month = 3;
   // offsetDateComponents.day=90;
    
    NSDate *yearLaterDate = [[NSCalendar currentCalendar] dateByAddingComponents:offsetDateComponents
                                                                          toDate:[NSDate date]
                                                                         options:0];
    
    return yearLaterDate;
}



#pragma mark - KDCalendarDelegate

-(void)calendarController:(KDCalendarView*)calendarViewController didSelectDay:(NSDate*)date
{
    //printf("prag");
    
    
    //NSString *myObjcData = [self.formatter stringFromDate:date];
    
    
    //  NSLog(@"bj%@",[self.formatter stringFromDate:date]);
    // self.inputTextField.text = [self.formatter stringFromDate:date];
    // NSString *hh = [self.formatter stringFromDate:date];
    
    
    
}






-(void)calendarController:(KDCalendarView*)calendarViewController didScrollToMonth:(NSDate*)date
{
    
    NSDateFormatter* headerFormatter = [[NSDateFormatter alloc] init];
    headerFormatter.dateFormat = @"MMMM, yyyy";
    
  
    
    self.monthDisplayedDayLabel.text = [headerFormatter stringFromDate:date];
    
    
    [self enableButtons:YES];
}



#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    self.inputTextField.text = @"";
    [self.inputTextField resignFirstResponder];
    
    return YES;
}


- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.inputTextField.text = @"";
}

- (void) textFieldDidEndEditing:(UITextField *)textField
{
    if(self.calendarView.dateSelected)
    {
        self.inputTextField.text = [self.formatter stringFromDate:self.calendarView.dateSelected];
    }
    else
    {
        self.inputTextField.text = @"";
    }
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    NSMutableString* mutableTextString = self.inputTextField.text.mutableCopy;
    
    [mutableTextString replaceCharactersInRange:range withString:string];
    
    if(range.length == 0) // we are adding a sinlge digit
    {
        NSCharacterSet* numericCharacterSet = [NSCharacterSet decimalDigitCharacterSet];
        if ([string rangeOfCharacterFromSet:numericCharacterSet].location == NSNotFound)
        {
            return NO;
        }
        
        if([string isEqualToString:@"."])
        {
            return NO;
        }
        
        if((range.location == 1 || range.location == 4) && range.location == self.inputTextField.text.length) // if we are appending at the end, help by putting the '.' dots
        {
            [mutableTextString appendString:@"."];
        }
    }
    
    
    
    self.inputTextField.text = mutableTextString;
    
    return NO;
}

#pragma mark - Stepping Months and Selecting Dates

- (IBAction)nextMonthPressed:(UIButton*)sender
{
    
    [self stepMonthInCalendarByValue:1];
}

- (IBAction)previousMonthPressed:(UIButton*)sender
{
    
    [self stepMonthInCalendarByValue:-1];
}

-(void)enableButtons:(BOOL)enable
{
    
    for (UIButton* button in @[self.previousMonthButton, self.nextMonthButton])
    {
        
        button.enabled = enable;
        
    }
}

- (void)stepMonthInCalendarByValue:(NSInteger)value
{
    
    
    [self enableButtons:NO];
    
    NSCalendar* calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSDateComponents* oneMonthAheadDateComponents = [[NSDateComponents alloc] init];
    oneMonthAheadDateComponents.month = value;
    
    NSDate* monthDisplayed = self.calendarView.monthDisplayed;
    NSDate* oneMonthLaterDate = [calendar dateByAddingComponents:oneMonthAheadDateComponents toDate:monthDisplayed options:0];
    
    [self.calendarView setMonthDisplayed:oneMonthLaterDate animated:YES];
}

- (IBAction)selectPressed:(UIButton *)sender {
    
    
    NSDate* dateParsed = [self.formatter dateFromString:self.inputTextField.text];
    if(!dateParsed)
    {
        [[[UIAlertView alloc] initWithTitle:@"Error Parsing Date"
                                    message:@"The date you entered is not valid!"
                                   delegate:self cancelButtonTitle:@"OK"
                          otherButtonTitles: nil] show];
        
    }
    else
    {
        
        
        [self.calendarView setDateSelected:dateParsed
                                  animated:YES];
        
        [self textFieldShouldReturn:self.inputTextField];
        
        
    }
}

@end

