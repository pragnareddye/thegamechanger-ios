//
//  BookingHistoryViewController.swift
//  The Game Changer
//
//  Created by local admin on 28/09/15.
//  Copyright (c) 2015 TheGameChanger. All rights reserved.
//

import Foundation
import UIKit

class BookingHistoryViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    //@IBOutlet weak var ActivityIndicator: UIActivityIndicatorView!
    
    @IBOutlet var tableView: UITableView!
    var arrayOfBookings: [BookingDetails] = [BookingDetails]()
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchBookingDetails()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        // Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
        // Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)
        return arrayOfBookings.count;
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let messageString1:String = arrayOfBookings[indexPath.row].timeString
        let messageString2:String = arrayOfBookings[indexPath.row].dates
        let messageString:String = "\(messageString1)\(messageString2)"
        //let currentCell = tableView.cellForRowAtIndexPath(indexPath) as UITableViewCell!
        
        // println(currentCell.textLabel.text)
        //let attributes = [UIFont(): UIFont.systemFontOfSize(15.0)]
        let attrString:NSAttributedString? = NSAttributedString(string: messageString)
        let rect:CGRect = attrString!.boundingRectWithSize(CGSizeMake(300.0,CGFloat.max), options: NSStringDrawingOptions.UsesLineFragmentOrigin, context:nil )//hear u will get nearer height not the exact value
        let requredSize:CGRect = rect
        return (requredSize.height+200)
        
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let bookingCell: BookingHistoryCell = tableView.dequeueReusableCellWithIdentifier("BookingCells") as! BookingHistoryCell
        print("Entered")
        if (indexPath.row % 2 != 0)
        {
            bookingCell.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.2);
        }
        
        //Initial labels in the cell
        let booking = arrayOfBookings[indexPath.row]
        bookingCell.setBookingCell(booking.bookingID, groundName: booking.groundName, game: booking.game, groundSize: booking.groundSize, dates: booking.dates, timeString: booking.timeString)
        return bookingCell
    }
    
    
    func fetchBookingDetails()
    {
        let post:NSString = ""
        let addr:NSString="bookings"
        let urlData: NSData? = DataObj.JSONFetch(post as String, Address: addr as String)
        if(urlData != nil)
        {
            let responseData:NSString  = NSString(data:urlData!, encoding:NSUTF8StringEncoding)!
            NSLog(responseData as String)
            do{
                let bookings:NSArray = try NSJSONSerialization.JSONObjectWithData(urlData!, options:NSJSONReadingOptions.MutableContainers)  as! NSArray
                let count:NSInteger=bookings.count
                var i:NSInteger=0;
                NSLog("count is \(count)")
                for i=0;i<count;i++
                {
                    
                    let dict_book :NSDictionary = bookings[i] as! NSDictionary
                    let ticketid:NSString = dict_book.objectForKey("ticket_id") as! NSString
                    let location:NSString = dict_book.objectForKey("ground") as! NSString
                    let game1:NSString = dict_book.objectForKey("sport") as! NSString
                    let mode:NSString=dict_book.objectForKey("portions") as! NSString
                    let bookingdate:NSString=dict_book.objectForKey("days") as! NSString
                    let slot:NSString=dict_book.objectForKey("timing") as! NSString
                    let booking1 = BookingDetails(bookingID: "\(ticketid)", groundName : "\(location)", game: "\(game1)", groundSize: "\(mode)", dates: "\(bookingdate)", timeString: "\(slot)")
                    arrayOfBookings.append(booking1)
                    
                }
                
            }
            catch{
                
            };
            
        }
        else
        {
            self.navigationController?.popViewControllerAnimated(true)
            if(Internet==0)
            {
                let alertView:UIAlertView = UIAlertView()
                alertView.title = "Error!"
                alertView.message = "Check your internet connection"
                alertView.delegate = LoginViewController.self
                alertView.addButtonWithTitle("OK")
                alertView.show()
            }
            else
            {
                let alertView:UIAlertView = UIAlertView()
                alertView.title = "Error!"
                alertView.message = "Try again"
                alertView.delegate = LoginViewController.self
                alertView.addButtonWithTitle("OK")
                alertView.show()
            }
        }
    }
}
class BookingDetails {
    var bookingID: String
    var groundName: String
    var game : String
    var groundSize : String
    var dates : String
    var timeString : String
    
    init(bookingID: String, groundName: String, game: String, groundSize: String, dates: String, timeString: String)
    {
        self.bookingID = bookingID
        self.groundName = groundName
        self.game = game
        self.groundSize = groundSize
        self.dates = dates
        self.timeString = timeString
    }
};