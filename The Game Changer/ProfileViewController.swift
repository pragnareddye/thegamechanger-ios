//
//  ProfileViewController.swift
//  The Game Changer
//
//  Created by swaroop on 9/26/15.
//  Copyright (c) 2015 TheGameChanger. All rights reserved.
//

import Foundation
import UIKit

class ProfileViewController: UIViewController {
    @IBOutlet var userName: UILabel!
    var email:NSString=""
    var phone:NSString=""
    var address:NSString=""
    var imgpath:NSString=""
    var imgpath2:NSString=""
    
    @IBOutlet var activityindicatorpic: UIActivityIndicatorView!
    @IBOutlet var addresst: UILabel!
    @IBOutlet var emailt: UILabel!
    @IBOutlet var phonet: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        emailt.text=prefs.objectForKey("email") as?  String
        phonet.text=prefs.objectForKey("mobile")as?  String
        addresst.text=prefs.objectForKey("dob") as?  String
        self.userName.text=prefs.objectForKey("name") as? String
        
    }
    
    @IBAction func logout(sender: UIButton) {
        
        let post:NSString = ""
        
        let addr:NSString="terminate"
        
        
        let urlData: NSData? = DataObj.JSONFetch(post as String, Address: addr as String)
        if(urlData != nil)
        {
            let responseData:NSString  = NSString(data:urlData!, encoding:NSUTF8StringEncoding)!
            NSLog(responseData as String)
            
            prefs.removeObjectForKey("UserName")
            prefs.removeObjectForKey("ISLOGGEDIN")
            prefs.synchronize()
            dispatch_async(dispatch_get_main_queue()) {
                self.performSegueWithIdentifier("logout", sender: self)
            }
            
        }
        else
        {
            prefs.removeObjectForKey("UserName")
            prefs.removeObjectForKey("password")
        }
        
        
        
        prefs.removeObjectForKey("UserName")
        prefs.removeObjectForKey("ISLOGGEDIN")
        prefs.synchronize()
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}