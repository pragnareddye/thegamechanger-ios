//
//  DataClass.swift
//  The Game Changer
//
//  Created by local admin on 26/02/16.
//  Copyright (c) 2016 TheGameChanger. All rights reserved.
//

import Foundation
import UIKit
var Internet:Int=1;

@objc public class DataClass:NSObject
{
    static var abspath:String="http://api.thegamechanger.club/"
    static var token:NSString=""
    var game:Int = -1
    var SportId:Int = 1
    var SportMin = 16
    var GroundMin = 16
    var GroundId = 0
    var SlotSize = 0.5
    var PortionsSelected = 16
    var SelectedGround:Int = 0
    static var Bulk:Int=0;
    var SlotsPicked:UInt64 = 100
    static var SelectedDatesString = ""
    static var SelectedDateString = ""
    static var AvailableDates:NSMutableArray!=nil
    
    var successString:NSString = ""
    static var SelectedDatesArray:NSMutableArray!=nil
    static var InternetFCS:Int=0;
    var TicketId:String = ""
    var coupon:String=""
    var night:Float=0
    var day:Float=0
    static func getObject()->DataClass
    {
        let DObj:DataClass=DataClass()
        return DObj
    }
    func JSONFetch(PostParameters:String , Address:String)->NSData!
    {
        
        NSLog("PostData: %@",PostParameters);
        let url:NSURL = NSURL(string: "\(DataClass.abspath)api/\(Address)?token=\(DataClass.token)")!
        let postData:NSData = PostParameters.dataUsingEncoding(NSASCIIStringEncoding)!
        let postLength:NSString = String( postData.length )
        let request:NSMutableURLRequest = NSMutableURLRequest(URL: url)
        request.HTTPMethod = "POST"
        request.HTTPBody = postData
        request.setValue(postLength as String, forHTTPHeaderField: "Content-Length")
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        
        
        var response: NSURLResponse?
        var urlData: NSData?
        do
        {
            urlData = try NSURLConnection.sendSynchronousRequest(request, returningResponse:&response)
        }
        catch
        {
            urlData = nil
        }
        
        
        
        if ( urlData != nil ) {
            Internet=1;
            let res = response as! NSHTTPURLResponse!;
            let responseData:NSString  = NSString(data:urlData!, encoding:NSUTF8StringEncoding)!
            NSLog(responseData as String)
            NSLog("Response code: %ld", res.statusCode);
            
            if (res.statusCode >= 200 && res.statusCode < 300)
            {
                let responseData:NSString  = NSString(data:urlData!, encoding:NSUTF8StringEncoding)!
                NSLog(responseData as String)
                return urlData!
            }
            else {
                
                
                do{
                    let jsonData:NSDictionary = try NSJSONSerialization.JSONObjectWithData(urlData!, options:NSJSONReadingOptions.MutableContainers)  as! NSDictionary
                    
                    let Message:NSString=jsonData.valueForKey("error") as! NSString
                    let message:NSString=Message.stringByReplacingOccurrencesOfString("_", withString: " " )
                    if(message=="token expired")
                    {
                        let username = prefs.objectForKey("UserName") as! NSString
                        let password = prefs.objectForKey("password") as! NSString
                        DataClass.sendsinfor(username, pass: password)
                    }
                    else
                    {
                        let alertView:UIAlertView = UIAlertView()
                        alertView.title = "Error!"
                        alertView.message = "\(message)"
                        alertView.addButtonWithTitle("OK")
                        alertView.show()
                    }
                }
                catch
                {
                    
                }
                
            }
            
        }
        else
        {
            Internet=0;
            
        }
        
        return nil;
        
    }
    
    
    //Login function
    static func sendsinfor( username:NSString, pass:NSString)->BooleanType
    {
        let post:NSString = "email=\(username)&password=\(pass)"
        
        let addr:NSString="authenticate"
        
        
        let urlData: NSData? = DataObj.JSONFetch(post as String, Address: addr as String)
        if(urlData != nil)
        {
            do{
                let jsonData:NSDictionary = try NSJSONSerialization.JSONObjectWithData(urlData!, options:NSJSONReadingOptions.MutableContainers)  as! NSDictionary
                
                DataClass.token=jsonData.valueForKey("token") as! NSString
                let me:NSDictionary=jsonData.valueForKey("me") as! NSDictionary
                prefs.setObject(username, forKey: "UserName")
                prefs.setObject(pass, forKey: "password")
                prefs.setObject(me.valueForKey("name"), forKey: "name")
                prefs.setObject(me.valueForKey("nickname"), forKey: "nickname")
                prefs.setObject(me.valueForKey("email"), forKey: "email")
                prefs.setObject(me.valueForKey("mobile"), forKey: "mobile")
                prefs.setObject(me.valueForKey("dob"), forKey: "dob")
                
                return true
            }
            catch{
                
                
            };
        }
        else
        {
            if(Internet==1)
            {
                prefs.removeObjectForKey("UserName")
                prefs.removeObjectForKey("password")
            }
            else
            {
                let alertView:UIAlertView = UIAlertView()
                alertView.title = "Retry"
                alertView.message = "Check your internet Connection"
                
                alertView.delegate = self
                alertView.addButtonWithTitle("OK")
                alertView.show()
            }
            
            
        }
        return false;
    }
    
    
}