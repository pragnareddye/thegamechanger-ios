//
//  BookingsCell.swift
//  The Game Changer
//
//  Created by local admin on 09/12/15.
//  Copyright (c) 2015 TheGameChanger. All rights reserved.
//

import Foundation
import UIKit

class BookingHistoryCell: UITableViewCell {
    @IBOutlet weak var bookingID: UILabel!
    @IBOutlet weak var groundName: UILabel!
    @IBOutlet weak var game: UILabel!
    @IBOutlet weak var groundSize: UILabel!
    @IBOutlet weak var dates: UILabel!
    @IBOutlet weak var timeSlots: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setBookingCell(bookingID: String, groundName: String, game: String, groundSize: String, dates: String, timeString: String)
    {
        self.bookingID.text = bookingID
        self.groundName.text = groundName
        self.game.text = game
        self.groundSize.text = groundSize
        self.dates.text = dates
        self.timeSlots.text = timeString
    }
}