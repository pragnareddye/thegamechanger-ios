
#import <UIKit/UIKit.h>

@interface KDMonthCollectionViewFlowLayout : UICollectionViewFlowLayout

- (instancetype) initWithCollectionViewSize:(CGSize)size andHeaderHeight:(CGFloat)height;

@end
